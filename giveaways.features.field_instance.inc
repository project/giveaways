<?php
/**
 * @file
 * giveaways.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function giveaways_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-giveaway-body'.
  $field_instances['node-giveaway-body'] = array(
    'bundle' => 'giveaway',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-giveaway-field_keys_per_day'.
  $field_instances['node-giveaway-field_keys_per_day'] = array(
    'bundle' => 'giveaway',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The number of items that should be dispensed per day so that there is a fair chance of claiming it for a longer duration.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_keys_per_day',
    'label' => 'Handouts per day',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-giveaway-field_keys_per_ip_address'.
  $field_instances['node-giveaway-field_keys_per_ip_address'] = array(
    'bundle' => 'giveaway',
    'default_value' => array(
      0 => array(
        'value' => 2,
      ),
    ),
    'deleted' => 0,
    'description' => 'The number of items that should be dispensed to a single IP address. This check makes it harder to abuse the system.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_keys_per_ip_address',
    'label' => 'Handouts Per IP Address',
    'required' => 1,
    'settings' => array(
      'max' => 10,
      'min' => 1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-giveaway-field_successfuly_giveaway_body'.
  $field_instances['node-giveaway-field_successfuly_giveaway_body'] = array(
    'bundle' => 'giveaway',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'You can specify parts of the handout in the message as #keyval_1#, #keyval_2# etc.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_successfuly_giveaway_body',
    'label' => 'Successful Giveaway Body',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Keys Per IP Address');
  t('Keys per day');
  t('Successful Giveaway Body');
  t('The number of items that should be dispensed per day so that there is a fair chance of claiming it for a longer duration.');
  t('The number of items that should be dispensed to a single IP address. This check makes it harder to abuse the system.');
  t('You can specify parts of the key in the message as #keyval_1#, #keyval_2# etc.');

  return $field_instances;
}
