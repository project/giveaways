<?php
/**
 * @file
 * giveaways.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function giveaways_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "data" && $api == "data_default") {
    return array("version" => "1");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function giveaways_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function giveaways_node_info() {
  $items = array(
    'giveaway' => array(
      'name' => t('Giveaway'),
      'base' => 'node_content',
      'description' => t('Use <em>giveaways</em> as a promotion to distribute discount codes/keys etc to participants'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
